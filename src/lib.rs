mod linked_list;

pub use linked_list::LinkedList;

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn list_u32() {
    let mut numbers: LinkedList<u32> = LinkedList::new();

    numbers.append(1);
    numbers.append(1);
    numbers.append(2);
    numbers.append(3);
    numbers.append(5);
    numbers.append(8);
    numbers.append(13);
    numbers.append(21);
    numbers.append(34);
    numbers.append(55);

    assert_eq!(numbers.length, 10);
  }

  #[test]
  fn list_str() {
    let mut numbers: LinkedList<&str> = LinkedList::new();

    numbers.append("One");
    numbers.append("Two");
    numbers.append("Three");
    numbers.append("Four");
    numbers.append("Five");

    assert_eq!(numbers.length, 5);
  }
}
