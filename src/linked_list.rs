use std::cell::RefCell;
use std::rc::Rc;

/// A new `Node` link.
pub type Link<T> = Option<Rc<RefCell<Node<T>>>>;

/// Node
#[derive(Clone, Debug)]
pub struct Node<T> {
  value: T,
  next: Link<T>,
}

impl<T> Node<T> {
  /// Create a new node.
  pub fn new(value: T) -> Rc<RefCell<Node<T>>> {
    Rc::new(RefCell::new(Node { value, next: None }))
  }
}

#[derive(Debug)]
pub struct LinkedList<T> {
  head: Link<T>,
  tail: Link<T>,
  pub length: usize,
}

impl<T> LinkedList<T> {
  /// Create a new empty `LinkedList`.
  ///
  /// # Example
  ///
  /// ```rust
  /// use list::LinkedList;
  ///
  /// let todo: LinkedList<String> = LinkedList::new();
  /// assert_eq!(todo.length, 0);
  /// ```
  pub fn new() -> LinkedList<T> {
    LinkedList {
      head: None,
      tail: None,
      length: 0,
    }
  }

  /// Add a new Node link to the end of the list.
  ///
  /// # Example
  ///
  /// ```rust
  /// use list::LinkedList;
  ///
  /// let mut todo: LinkedList<String> = LinkedList::new();
  ///
  /// todo.append(String::from("Eat"));
  /// todo.append(String::from("Sleep"));
  /// todo.append(String::from("Code"));
  ///
  /// assert_eq!(todo.length, 3);
  /// ```
  pub fn append(&mut self, value: T) {
    let new = Node::new(value);

    match self.tail.take() {
      Some(old) => old.borrow_mut().next = Some(new.clone()),
      None => self.head = Some(new.clone()),
    };

    self.length += 1;
    self.tail = Some(new);
  }
}
